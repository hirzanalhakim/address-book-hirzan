import React from "react";
import { withTranslation } from "react-i18next";
import { withStyles } from "@material-ui/core/styles";
import LightIcon from "@material-ui/icons/Brightness7";
import DarkIcon from "@material-ui/icons/Brightness4";
import SearchIcon from "@material-ui/icons/Search";
import {
  AppBar,
  Button,
  ButtonGroup,
  IconButton,
  InputBase,
  Toolbar,
  Typography
} from "@material-ui/core";
import styles from "./Header.styles";

const Header = props => {
  const { classes, fnChangeMode, mode, fnSearch, fnChangeLanguage, t } = props;
  return (
    <AppBar position="fixed">
      <Toolbar>
        <Typography className={classes.title} variant="h6" noWrap>
          {t("title")}
        </Typography>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            onChange={fnSearch}
            placeholder={`${t("search")}...`}
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput
            }}
          />
        </div>
        <div className={classes.grow} />

        <ButtonGroup variant="text" aria-label="text primary button group">
          <Button
            onClick={() => fnChangeLanguage("en")}
            disabled={mode.lang === "en"}
          >
            En
          </Button>
          <Button
            onClick={() => fnChangeLanguage("id")}
            disabled={mode.lang === "id"}
          >
            In
          </Button>
        </ButtonGroup>
        <IconButton aria-label="delete" onClick={fnChangeMode}>
          {mode.type === "light" ? (
            <DarkIcon fontSize="large" />
          ) : (
            <LightIcon fontSize="large" />
          )}
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default withStyles(styles)(withTranslation("translation")(Header));

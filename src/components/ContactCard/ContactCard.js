import React, { useState } from "react";
import { withTranslation } from "react-i18next";
import { withStyles } from "@material-ui/core/styles";
import {
  Grid,
  Card,
  CardContent,
  CardMedia,
  Grow,
  Slide,
  IconButton,
  Typography,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import { isMobile } from "react-device-detect";
import styles from "./ContactCard.styles";
import defaultAvatar from "../../assets/image/default-avatar.png";

const ContactCard = (props) => {
  const { classes, data, fnRemove, fnSelect, t } = props;
  const { name, picture, location } = data;
  const [isHover, setHover] = useState(false);

  const deleteCard = (e) => {
    e.stopPropagation();
    fnRemove(data);
  };

  return (
    <Grid item md={3} sm={6} xs={12}>
      <Grow in={true}>
        <Card
          className={classes.card}
          onMouseOver={() => setHover(true)}
          onMouseLeave={() => setHover(false)}
          onClick={() => fnSelect(data)}
          raised={isHover}
        >
          <CardMedia
            className={classes.cover}
            image={(picture && picture.large) || defaultAvatar}
            title={(name && name.first) || ""}
          />
          <div className={classes.details}>
            <CardContent className={classes.content}>
              <Typography noWrap className={classes.title}>
                {name && name.first} {name && name.last}
              </Typography>
              <Grid container direction="row">
                <Grid item xs={2}>
                  <Typography className={classes.font12}>
                    {t("street")}
                  </Typography>
                  <Typography className={classes.font12}>
                    {t("city")}
                  </Typography>
                </Grid>
                <Grid item xs={10}>
                  <Typography className={classes.font12} noWrap>
                    : {location && location.street && location.street.name}
                  </Typography>
                  <Typography className={classes.font12} noWrap>
                    : {location && location.city}
                  </Typography>
                </Grid>
              </Grid>
              {isMobile && (
                <div className={classes.rightOverlay} onClick={deleteCard}>
                  <IconButton aria-label="delete">
                    <DeleteIcon />
                  </IconButton>
                </div>
              )}
              {!isMobile && isHover && (
                <Slide direction="down" in={true}>
                  <div className={classes.rightOverlay} onClick={deleteCard}>
                    <IconButton aria-label="delete">
                      <DeleteIcon />
                    </IconButton>
                  </div>
                </Slide>
              )}
            </CardContent>
          </div>
        </Card>
      </Grow>
    </Grid>
  );
};

export default withStyles(styles)(withTranslation("translation")(ContactCard));

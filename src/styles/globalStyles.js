const theme = {
  palette: {
    primary: {
      main: "#0097a7"
    },
    secondary: {
      main: "#f50057"
    },
    type: "light"
  }
};

export default theme;

import { createStore, applyMiddleware } from "redux";
import { logger } from "redux-logger";
import thunk from "redux-thunk";
import reducers from "./action-reducers";

//persist
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web

const persistConfig = {
  key: "gc-test",
  storage: storage,
  whitelist: ["mode"]
};

const middlewares = [thunk];
if (process.env.NODE_ENV === "development") {
  middlewares.push(logger);
}

const persistRdx = persistReducer(persistConfig, reducers);
export const store = createStore(persistRdx, applyMiddleware(...middlewares));
export const persistor = persistStore(store);

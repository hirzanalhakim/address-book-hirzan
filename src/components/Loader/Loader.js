import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { CircularProgress, Grid } from "@material-ui/core";
import styles from "./Loader.styles";

const Loader = props => {
  const { classes } = props;
  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      className={classes.loaderContainer}
    >
      <CircularProgress size={35} />
    </Grid>
  );
};

export default withStyles(styles)(Loader);

import React from "react";
import { Grid, Typography } from "@material-ui/core";
import { withTranslation } from "react-i18next";

const NotFound = props => {
  const { t } = props;
  return (
    <Grid container justify="center" alignItems="center">
      <Typography align="center" color="textSecondary">
        {t("pageNotFound")}
      </Typography>
    </Grid>
  );
};

export default withTranslation()(NotFound);

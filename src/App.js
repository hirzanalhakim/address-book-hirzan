import React, { Component } from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { connect } from "react-redux";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { withTranslation } from "react-i18next";
import { CssBaseline } from "@material-ui/core";
import globalStyles from "./styles/globalStyles";
import { Home, NotFound } from "./containers";

class App extends Component {
  componentDidMount() {
    this.changeLanguage();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.mode !== this.props.mode) {
      this.changeLanguage();
    }
  }

  changeLanguage = () => {
    const { i18n, mode } = this.props;
    i18n.changeLanguage(mode.lang);
  };
  render() {
    const { mode } = this.props;
    const theme = {
      ...globalStyles,
      palette: { ...globalStyles.palette, type: mode.type }
    };

    const themeConfig = createMuiTheme(theme);
    return (
      <Router>
        <ThemeProvider theme={themeConfig}>
          <CssBaseline />
          <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="*">
              <NotFound />
            </Route>
          </Switch>
        </ThemeProvider>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return {
    mode: state.mode
  };
};

export default withTranslation()(connect(mapStateToProps, null)(App));

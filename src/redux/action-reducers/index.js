import { combineReducers } from "redux";

import mode from "./mode/modeRdx.js";
import contact from "./contact/contactRdx.js";

// export action/function
export { changeMode, changeLanguage } from "./mode/modeRdx.js";
export {
  addContact,
  removeContact,
  searchContact
} from "./contact/contactRdx.js";

export default combineReducers({ mode, contact });

import React from "react";
import { withTranslation } from "react-i18next";
import { withStyles } from "@material-ui/core/styles";
import {
  Avatar,
  Dialog,
  DialogActions,
  DialogContent,
  Grid,
  IconButton,
  Slide,
  Typography
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import defaultAvatar from "../../assets/image/default-avatar.png";
import styles from "./ContactDetail.styles";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
const ContactDetail = props => {
  const { classes, data, fnClose, t } = props;
  return (
    <Dialog
      open={data ? true : false}
      TransitionComponent={Transition}
      keepMounted
      onClose={fnClose}
    >
      <DialogActions>
        <IconButton onClick={fnClose} color="primary">
          <CloseIcon fontSize="small" />
        </IconButton>
      </DialogActions>
      <DialogContent className={classes.content}>
        <Avatar
          className={classes.profilePict}
          src={(data && data.picture && data.picture.large) || defaultAvatar}
        />
        <Typography className={classes.name} align="center">
          {`${data && data.name && data.name.first} ${data &&
            data.name &&
            data.name.last}`}
        </Typography>

        <Grid container direction="row">
          <Grid item xs={3}>
            <Typography className={classes.text}>{t("email")}</Typography>
          </Grid>
          <Grid item xs={9}>
            <Typography className={classes.text}>
              {`: ${data && data.email}`}
            </Typography>
          </Grid>
        </Grid>

        <Grid container direction="row">
          <Grid item xs={3}>
            <Typography className={classes.text}>{t("phone")}</Typography>
          </Grid>
          <Grid item xs={9}>
            <Typography className={classes.text}>
              {`: ${data && data.phone}`}
            </Typography>
          </Grid>
        </Grid>

        <Grid container direction="row">
          <Grid item xs={3}>
            <Typography className={classes.text}>{t("cell")}</Typography>
          </Grid>
          <Grid item xs={9}>
            <Typography className={classes.text}>
              {`: ${data && data.cell}`}
            </Typography>
          </Grid>
        </Grid>

        <Grid container direction="row">
          <Grid item xs={3}>
            <Typography className={classes.text}>{t("street")}</Typography>
          </Grid>
          <Grid item xs={9}>
            <Typography className={classes.text}>
              {`: ${data &&
                data.location &&
                data.location.street &&
                data.location.street.name} ${data &&
                data.location &&
                data.location.street &&
                data.location.street.number}`}
            </Typography>
          </Grid>
        </Grid>

        <Grid container direction="row">
          <Grid item xs={3}>
            <Typography className={classes.text}>{t("city")}</Typography>
          </Grid>
          <Grid item xs={9}>
            <Typography className={classes.text}>
              {`: ${data && data.location && data.location.city}`}
            </Typography>
          </Grid>
        </Grid>

        <Grid container direction="row">
          <Grid item xs={3}>
            <Typography className={classes.text}>{t("state")}</Typography>
          </Grid>
          <Grid item xs={9}>
            <Typography className={classes.text}>
              {`: ${data && data.location && data.location.state}`}
            </Typography>
          </Grid>
        </Grid>

        <Grid container direction="row">
          <Grid item xs={3}>
            <Typography className={classes.text}>{t("country")}</Typography>
          </Grid>
          <Grid item xs={9}>
            <Typography className={classes.text}>
              {`: ${data && data.location && data.location.country}`}
            </Typography>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
};

export default withStyles(styles)(
  withTranslation("translation")(ContactDetail)
);

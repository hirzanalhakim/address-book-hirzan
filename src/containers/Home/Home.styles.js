export default theme => ({
  root: {
    flexGrow: 1,
    justifyContent: "center",
    marginTop: theme.spacing(10)
  },
  gridContainer: {
    width: "100% !important",
    margin: "0 !important"
  },
  notFoundContainer: {
    height: "80vh"
  }
});

import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import axios from "axios";
import { connect } from "react-redux";
import { withStyles, Grid, Typography } from "@material-ui/core";
import { Header, ContactCard, Loader, ContactDetail } from "../../components";
import {
  addContact,
  changeLanguage,
  changeMode,
  removeContact,
  searchContact
} from "../../redux/action-reducers";
import styles from "./Home.styles";

class Home extends Component {
  state = {
    isLoading: false,
    page: 0,
    limitPage: 20,
    selectedCard: null
  };

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
    this.getAddress();
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  //this function will get address book from api
  getAddress = async () => {
    const { page } = this.state;
    this.setState({ isLoading: true });
    try {
      const response = await axios.get(
        `https://randomuser.me/api/?page=${page}&results=50&seed=abc&exc=login,id,registered`
      );
      if (response && response.data) {
        this.props.addContact(response.data.results);
      }
    } catch (error) {
      console.error(error);
    } finally {
      this.setState({ isLoading: false });
    }
  };

  //function for handleScroll
  handleScroll = async () => {
    const { isLoading, page, limitPage } = this.state;
    if (
      window.innerHeight + document.documentElement.scrollTop ===
        document.documentElement.offsetHeight &&
      !isLoading &&
      page < limitPage
    ) {
      // if the condition is true, set page to +1, then using callback to request more
      await this.setState(
        prevState => ({ page: prevState.page + 1 }),
        () => this.getAddress()
      );
    }
  };

  // function when user click card
  setSelectedCard = data => {
    //set selectedCard = the data that user clicked
    this.setState({ selectedCard: data });
  };

  //function for close modal
  handleCloseModal = () => {
    // for closing modal, just set selectedcard to be null,
    // bcs the modal just open when selectedCard is not null
    this.setState({ selectedCard: null });
  };

  handleSearch = event => {
    const { searchContact } = this.props;
    const text = event.target.value;
    searchContact(text);
  };

  render() {
    const {
      classes,
      mode,
      changeMode,
      contact,
      removeContact,
      changeLanguage,
      t
    } = this.props;
    const { isLoading, selectedCard, page, limitPage } = this.state;
    return (
      <>
        <Header
          mode={mode}
          fnChangeMode={changeMode}
          fnSearch={this.handleSearch}
          fnChangeLanguage={changeLanguage}
        />
        <div className={classes.root}>
          <Grid container spacing={3} className={classes.gridContainer}>
            {contact.isFiltering
              ? contact.filteredData &&
                contact.filteredData.length > 0 &&
                contact.filteredData.map((data, idx) => (
                  <ContactCard
                    key={idx}
                    data={data}
                    fnRemove={removeContact}
                    fnSelect={this.setSelectedCard}
                  />
                ))
              : contact.data &&
                contact.data.length > 0 &&
                contact.data.map((data, idx) => (
                  <ContactCard
                    key={idx}
                    data={data}
                    fnRemove={removeContact}
                    fnSelect={this.setSelectedCard}
                  />
                ))}
          </Grid>
          {/* if when filtering doesn't match with our data */}
          {contact.isFiltering && contact.filteredData.length < 1 && (
            <Grid
              container
              justify="center"
              alignItems="center"
              className={classes.notFoundContainer}
            >
              <Typography align="center" color="textSecondary">
                {t("notFound")}
              </Typography>
            </Grid>
          )}
          {isLoading && <Loader />}
          <ContactDetail data={selectedCard} fnClose={this.handleCloseModal} />
          {page === limitPage && !isLoading && (
            <>
              <hr />
              <Typography align="center" color="textSecondary">
                {t("endPage")}
              </Typography>
            </>
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    contact: state.contact,
    mode: state.mode
  };
};

export default withStyles(styles)(
  connect(mapStateToProps, {
    addContact,
    changeLanguage,
    changeMode,
    removeContact,
    searchContact
  })(withTranslation("translation")(Home))
);

import { CHANGE_MODE, CHANGE_LANGUAGE } from "../../action.type";

const initialState = {
  type: "light",
  lang: "en"
};

export default function reducers(state = initialState, actions) {
  const { type, payload } = actions;
  switch (type) {
    case CHANGE_MODE:
      return {
        ...state,
        type: state.type === "light" ? "dark" : "light"
      };
    case CHANGE_LANGUAGE:
      return {
        ...state,
        lang: payload
      };
    default:
      return state;
  }
}
export const changeMode = value => {
  return dispatch => {
    dispatch({ type: CHANGE_MODE });
  };
};

export const changeLanguage = value => {
  return dispatch => {
    dispatch({ type: CHANGE_LANGUAGE, payload: value });
  };
};

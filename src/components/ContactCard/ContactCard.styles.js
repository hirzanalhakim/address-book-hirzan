export default theme => ({
  card: {
    display: "flex",
    width: "100%",
    cursor: "pointer",
    position: "relative"
  },
  details: {
    display: "flex",
    width: "100%",
    flexDirection: "column"
  },
  content: {
    width: "100%",
    padding: "8px !important"
  },
  cover: {
    maxWidth: "25%",
    minWidth: "25%"
  },
  title: {
    fontSize: 14,
    fontWeight: "bold"
  },
  font12: {
    fontSize: 12
  },
  rightOverlay: {
    position: "absolute",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    top: 0,
    bottom: 0,
    right: 0,
    backgroundColor: theme.palette.secondary.main,
    overflow: "hidden",
    width: 30,
    height: "100%",
    opacity: 0.6
  }
});

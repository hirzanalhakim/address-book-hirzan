import {
  ADD_CONTACT,
  ADD_CONTACT_SUCCESS,
  CONTACT_ERROR,
  REMOVE_CONTACT,
  SEARCH_CONTACT
} from "../../action.type";

const initialState = {
  data: [],
  isLoading: false,
  isError: false,
  isFiltering: false,
  filteredData: []
};

export default function reducers(state = initialState, actions) {
  const { payload, type } = actions;
  switch (type) {
    case ADD_CONTACT:
      return { ...state, data: state.data, isLoading: true, isError: false };
    case ADD_CONTACT_SUCCESS:
      return {
        ...state,
        data: [...state.data, ...payload],
        isLoading: false,
        isError: false
      };
    case REMOVE_CONTACT:
      // delete contact at state.data
      const newContact = [...state.data];
      const index = newContact.indexOf(payload);
      if (index > -1) {
        newContact.splice(index, 1);
      }

      // delete contact at state.filteredData
      const newFilteredData = [state.filteredData];
      const indexFiltered = newFilteredData.indexOf(payload);
      if (index > -1) {
        newFilteredData.splice(indexFiltered, 1);
      }
      return {
        ...state,
        data: newContact,
        filteredData: newFilteredData,
        isLoading: false,
        isError: false
      };
    case SEARCH_CONTACT:
      return {
        ...state,
        isFiltering: payload.length > 0,
        filteredData: state.data.filter(data => {
          const text = payload.toLowerCase();
          const username = `${data.name.first} ${data.name.last}`;
          return username.toLowerCase().includes(text);
        })
      };
    case CONTACT_ERROR:
      return { ...state, data: state.contact, isLoading: false, isError: true };
    default:
      return state;
  }
}
export const addContact = value => {
  return dispatch => {
    dispatch({ type: ADD_CONTACT_SUCCESS, payload: value });
  };
};

export const removeContact = value => {
  return dispatch => {
    dispatch({ type: REMOVE_CONTACT, payload: value });
  };
};

export const searchContact = value => {
  return dispatch => {
    dispatch({ type: SEARCH_CONTACT, payload: value });
  };
};

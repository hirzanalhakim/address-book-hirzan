export default theme => ({
  content: {
    minWidth: 330,
    width: "100%",
    padding: "0px 20px 20px 20px"
  },
  profilePict: {
    margin: "10px auto",
    width: 120,
    height: 120
  },
  name: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 14
  },
  text: {
    fontSize: 14
  }
});

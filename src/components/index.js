import ContactCard from "./ContactCard/ContactCard";
import ContactDetail from "./ContactDetail/ContactDetail";
import Header from "./Header/Header";
import Loader from "./Loader/Loader";

export { ContactCard, ContactDetail, Header, Loader };

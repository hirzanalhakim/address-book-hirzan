This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to run this project :

1. Clone this project
2. Go to the project's folder that has been Clone
3. yarn install / npm install 
4. yarn start / npm run start

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

